// Package that manages the serial communication protocol with the McLane
// Profiler Controller (MPC). The MPC must be running firmware version
// 5.23 or later.
//
// This package is part of the RSN Deep Profiler project.
package mpc

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ProfileType uint8

// Profile type codes.
const (
	PROFILE_DOWN       ProfileType = 0
	PROFILE_UP         ProfileType = 1
	PROFILE_STATIONARY ProfileType = 2
	PROFILE_DOCKING    ProfileType = 3
	PROFILE_INVALID    ProfileType = 255
)

func (p ProfileType) String() string {
	switch p {
	case PROFILE_DOWN:
		return "down"
	case PROFILE_UP:
		return "up"
	case PROFILE_STATIONARY:
		return "stationary"
	case PROFILE_DOCKING:
		return "docking"
	default:
		return "invalid"
	}
}

// Interface to the MPC
type MPC struct {
	rw      io.ReadWriter
	debug   bool
	wg      sync.WaitGroup
	timeout time.Duration
}

// Error triggered by an invalid response
type ProtocolError struct {
	msg string
}

func (e *ProtocolError) Error() string {
	return e.msg
}

// NewMPC returns a new MPC attached to an I/O port.
func NewMPC(port io.ReadWriter, timeout time.Duration) *MPC {
	return &MPC{
		rw:      port,
		timeout: timeout}
}

// Enable or disable debugging output.
func (m *MPC) SetDebug(state bool) {
	m.debug = state
}

// SetTimeout sets the maximum time to wait for message from the MPC
func (m *MPC) SetTimeout(timeout time.Duration) {
	m.timeout = timeout
}

// Send sends a message to the MPC. A message consists of a command
// followed by zero or more arguments. The args values are converted to
// strings before sending. It returns any error that occurred.
func (m *MPC) Send(cmd string, args ...interface{}) error {
	m.rw.Write([]byte(cmd))
	_, err := m.rw.Write([]byte(" "))
	if err == nil {
		n := len(args)
		for i, arg := range args {
			switch v := arg.(type) {
			case MpcMarshaler:
				var b []byte
				b, err = v.MarshalMpc()
				if err == nil {
					_, err = m.rw.Write(b)
				}
			case string:
				_, err = m.rw.Write([]byte(v))
			case []byte:
				_, err = m.rw.Write(v)
			case ProfileType:
				_, err = m.rw.Write([]byte(strconv.FormatUint(uint64(v), 10)))
			case fmt.Stringer:
				_, err = m.rw.Write([]byte(v.String()))
			case int64:
				_, err = m.rw.Write([]byte(strconv.FormatInt(v, 10)))
			case uint64:
				_, err = m.rw.Write([]byte(strconv.FormatUint(v, 10)))
			case uint16:
				_, err = m.rw.Write([]byte(strconv.FormatUint(uint64(v), 10)))
			default:
				err = fmt.Errorf("Unknown arg type: %v", arg)
			}
			if err != nil {
				return err
			}

			if i < (n - 1) {
				m.rw.Write([]byte(" "))
			}
		}
		_, err = m.rw.Write([]byte("\r\n"))
	}
	return err
}

func readLine(ctx context.Context, r io.Reader) (string, error) {
	char := make([]byte, 1)
	rec := make([]byte, 0, 128)
	const endChar = '\n'

	for {
		select {
		case <-ctx.Done():
			return string(rec), ctx.Err()
		default:
		}

		n, err := r.Read(char)
		if n == 0 || errors.Is(err, io.EOF) {
			continue
		}

		if err != nil {
			return string(rec), fmt.Errorf("looking for END: %w", err)
		}

		if char[0] == endChar {
			break
		}

		rec = append(rec, char[0])

	}

	return string(rec), nil
}

// Recv receives a message from the MPC. The return values are
// the command string, a list of one or more parameters, and
// any error that occurred.
func (m *MPC) Recv() (Message, error) {
	var (
		err  error
		ival int64
		fval float64
	)
	msg := Message{Params: make([]interface{}, 0)}
	ctx, cancel := context.WithTimeout(context.Background(), m.timeout)
	defer cancel()

	text, err := readLine(ctx, m.rw)
	if m.debug {
		log.Printf("RECV: %q", text)
	}

	if err != nil {
		return msg, err
	}

	// Remove any leading/trailing nuls and white-space, then split
	// the command-code from the parameters.
	fields := strings.SplitN(strings.Trim(text, " \t\r\n\x00>"), " ", 2)
	msg.Cmd = fields[0]

	if msg.Cmd == "MMPERR" {
		msg.Params = append(msg.Params, fields[1])
	} else if len(fields) > 1 {

		// In most messages, the parameters are separated with a comma
		// *and* a space (!!), however, sometimes they are only separated
		// by a space. Try to handle both cases.
		var sep string
		if strings.Contains(fields[1], ",") {
			sep = ", "
		} else {
			sep = " "
		}

		for _, s := range strings.Split(fields[1], sep) {
			ival, err = strconv.ParseInt(s, 0, 64)
			if err == nil {
				msg.Params = append(msg.Params, ival)
			} else {
				fval, err = strconv.ParseFloat(s, 64)
				if err == nil {
					msg.Params = append(msg.Params, fval)
				} else {
					msg.Params = append(msg.Params, s)
				}
			}
		}
	}
	return msg, nil
}

// Return two channels; one for Messages and one for any error that
// occurs. Timeout errors from the underlying serial port can be ignored
// by setting the ignore_timeo argument to true. The message channel will
// produce output until either a message code (Cmd) matching sentinel is
// received from the MPC, or the Context is cancelled.
func (m *MPC) Reader(ctx context.Context, sentinel string,
	ignoreTimeo bool) (<-chan Message, <-chan error) {
	out := make(chan Message, 1)
	errc := make(chan error, 1)
	m.wg.Add(1)

	go func() {
		defer close(out)
		defer m.wg.Done()
		for {
			msg, err := m.Recv()
			if errors.Is(err, context.DeadlineExceeded) && ignoreTimeo {
				err = nil
			}

			if err != nil {
				errc <- err
				return
			}

			select {
			case out <- msg:
			case <-ctx.Done():
				errc <- ctx.Err()
				return
			}
			if msg.Cmd == sentinel {
				errc <- nil
				return
			}
		}
	}()
	return out, errc
}

// Wait for the Reader to finish
func (m *MPC) ReaderWait() {
	m.wg.Wait()
}

// Check for an MMPRDY message which indicates that the MPC
// is ready to receive a command.
func (m *MPC) IsReady() (bool, error) {
	var status bool
	var err error

	status = false
	msg, err := m.Recv()
	if err == nil {
		status = (msg.Cmd == "MMPRDY")
	}
	return status, err
}

// Wait for an MMPRDY message, all other messages are ignored.
func (m *MPC) WaitUntilReady() error {
	_, err := m.WaitForMsg("MMPRDY")
	return err
}

// WaitForMsg waits for val or for the timeout to expire.
func (m *MPC) WaitForMsg(val string) (Message, error) {
	var (
		err error
		msg Message
	)

	for {
		msg, err = m.Recv()
		if err != nil || msg.Cmd == val {
			break
		}
	}

	return msg, err
}

func (m *MPC) getResponse(expect string, wait bool) (Message, error) {
	msg, err := m.Recv()
	if err != nil {
		return msg, err
	}

	if msg.Cmd != expect {
		errmsg := fmt.Sprintf("Expected %q got %q", expect, msg.Cmd)
		return msg, &ProtocolError{msg: errmsg}
	}

	if wait {
		err = m.WaitUntilReady()
	}

	return msg, err
}

// SetRamp sets the motor ramp-up time in seconds
func (m *MPC) SetRamp(seconds int) error {
	if seconds < 2 || seconds > 90 {
		return fmt.Errorf("Invalid ramp duration: %d", seconds)
	}
	err := m.Send("REQRMP", int64(seconds))
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKRMP", true)
	return err
}

// SetClock sets the clock on the MPC to the current time
// on the host computer. Offset seconds will be added to
// then time before sending it.
func (m *MPC) SetClock(offset int) error {
	err := m.Send("REQCLK", time.Now().Unix()+int64(offset))
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKCLK", true)
	return err
}

// Set the profile warm-up time.
func (m *MPC) SetWarmUp(seconds int) error {
	err := m.Send("REQWRM", int64(seconds), int64(seconds))
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKWRM", true)
	return err
}

// Get the profile warm-up time.
func (m *MPC) GetWarmUp() (int, error) {
	warmup := int(0)
	err := m.Send("REQWRM")
	if err != nil {
		return warmup, err
	}

	msg, err := m.getResponse("ACKWRM", true)
	if err != nil {
		return warmup, err
	}
	warmup = int(msg.Params[0].(int64))
	return warmup, err
}

// Set the profile back-track parameters.
func (m *MPC) SetBacktrack(cfg BacktrackConfig) error {
	err := m.Send("REQBAK", int64(cfg.Iterations),
		int64(cfg.Rate), int64(cfg.Time))
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKBAK", true)
	return err
}

// Get the profile back-track parameters.
func (m *MPC) GetBacktrack() (BacktrackConfig, error) {
	cfg := BacktrackConfig{}
	err := m.Send("REQBAK")
	if err != nil {
		return cfg, err
	}

	msg, err := m.getResponse("ACKBAK", true)
	if err != nil {
		return cfg, err
	}
	err = UnmarshalMpc(msg, &cfg)
	return cfg, err
}

// Set the parameters for the next profile. Return the profile
// start time (usually "now") and any error that occured.
func (m *MPC) SetProfile(cfg ProfileConfig) (time.Time, error) {
	tstart := cfg.Tstart()
	err := m.Send("REQPRF", cfg)
	if err != nil {
		return tstart, err
	}

	msg, err := m.getResponse("ACKPRF", true)
	if err != nil {
		return tstart, err
	}

	resp := ProfileConfig{}
	err = UnmarshalMpc(msg, &resp)
	if err != nil {
		return tstart, err
	}

	if resp.Direction != cfg.Direction {
		return tstart, &ProtocolError{
			msg: fmt.Sprintf("Profile mismatch: %#v", resp),
		}
	}

	return tstart, err
}

// Start the most recently configured profile.
func (m *MPC) StartProfile() error {
	err := m.Send("REQCNT")
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKCNT", false)
	return err
}

// Send the Profiler to the Dock
func (m *MPC) GotoDock() error {
	err := m.Send("REQCHG")
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKCHG", false)
	return err
}

// Leave the Dock
func (m *MPC) LeaveDock() error {
	err := m.Send("REQUND")
	if err != nil {
		return err
	}

	_, err = m.getResponse("ACKUND", true)
	return err
}
