package mpc

import (
	"fmt"
	"time"
)

// Message sent by the MPC
type Message struct {
	// Command code
	Cmd string
	// List of arbitrary parameters
	Params []interface{}
}

type MessageDesc map[string][]string

// Map a message command code to the message fields.
var Messages = MessageDesc{
	"MMPPOS": {"pnum", "current", "vbatt", "pressure", "mode"},
	"MMPPRF": {"t", "pnum", "mode"},
	"MMPEND": {"t", "pnum", "status"},
	"MMPOBS": {"t", "pr", "iter"},
	"MMPDCK": {"t", "pr"},
}

type Mapper interface {
	AsMap() map[string]interface{}
}

// ProfStart represents an MMPPRF message
type ProfStart struct {
	T    int64
	Pnum int64
	Mode ProfileType
}

func (p ProfStart) AsMap() map[string]interface{} {
	m := make(map[string]interface{})
	m["t"] = p.T
	m["pnum"] = p.Pnum
	m["mode"] = p.Mode.String()
	return m
}

// ProfEnd represents an MMPEND message. Below is the list of
// status codes supplied by McLane:
/*
   enum
{
	SMOOTH_RUNNING,      //  0
	MISSION_COMPLETE,    //  1
	OPERATOR_CTRL_C,     //  2
	TT8_COMM_FAILURE,    //  3
	CTD_COMM_FAILURE,    //  4
	ACM_COMM_FAILURE,    //  5
	TIMER_EXPIRED,       //  6
	MIN_BATTERY,         //  7
	MAX_MOTOR_CURRENT,   //  9
	TOP_PRESSURE,        // 13
	BOTTOM_PRESSURE,     // 14
	PRESSURE_RATE_ZERO,  // 15
	STOP_NULL,           // 16
	FLASH_CARD_FULL,     // 17
	FILE_SYSTEM_FULL,    // 18
	TOO_MANY_OPEN_FILES, // 19
	STATIONARY_EXPIRED   // 21
	DOCK_PROXIMITY       // 22
}

*/
type ProfEnd struct {
	T      int64
	Pnum   int64
	Status int64
}

func (p ProfEnd) AsMap() map[string]interface{} {
	m := make(map[string]interface{})
	m["t"] = p.T
	m["pnum"] = p.Pnum
	m["status"] = p.Status
	return m
}

// ProfDock represents an MMPDCK message
type ProfDock struct {
	T        int64
	Pressure float64
}

func (p ProfDock) AsMap() map[string]interface{} {
	m := make(map[string]interface{})
	m["t"] = p.T
	m["pr"] = p.Pressure
	return m
}

// ProfObs represents and MMPOBS message
type ProfObs struct {
	T        int64
	Pressure float64
	Iter     int64
}

func (p ProfObs) AsMap() map[string]interface{} {
	m := make(map[string]interface{})
	m["t"] = p.T
	m["pr"] = p.Pressure
	m["iter"] = p.Iter
	return m
}

type Position struct {
	// MPC timestamp
	T int64
	// Profile number
	Pnum int64
	// Motor current in mA
	Current int64
	// Battery voltage in V
	Vbatt float64
	// Pressure in dBar
	Pressure float64
	// Profile type code
	Mode ProfileType
}

func (p Position) AsMap() map[string]interface{} {
	m := make(map[string]interface{})
	m["pnum"] = p.Pnum
	m["current"] = p.Current
	m["vbatt"] = p.Vbatt
	m["pressure"] = p.Pressure
	m["mode"] = p.Mode.String()

	return m
}

// Configuration parameters for a profile. For the most part, these are
// the exact parameters passed in the REQPRF command.
type ProfileConfig struct {
	// Profile direction; 0=dive, 1=rise, 2=stationary
	Direction ProfileType
	// Start time of the profile. If <= 0, the profile is started
	// immediately, if set to "N" (N > 0), the profile is started on the
	// next N-second interval (e.g. 3600 will start the profile at the top
	// of the next hour.
	StartTime int64
	// Shallow depth goal in dbar
	Shallow uint16
	// Deep depth goal in dbar
	Deep uint16
	// Stop-check (depth check) interval in seconds
	StopCheck uint16
	// Profile time limit in seconds
	TimeLimit uint16
	// Shallow error-band in dbar
	ShallowErr uint16
	// Deep error-band in dbar
	DeepErr uint16
	// How may times should this profile be "re-queued"?
	Requeue uint32 `mpc:"-"`
}

func (p ProfileConfig) String() string {
	return fmt.Sprintf("Type=%s, Tstart=%d, Shallow=%d dbar, Deep=%d dbar, Tlimit=%d s",
		p.Direction,
		p.StartTime,
		p.Shallow,
		p.Deep,
		p.TimeLimit)
}

func (p ProfileConfig) Tstart() time.Time {
	t := time.Now()
	if p.StartTime > 0 {
		// Round-up to the nearest multiple of StartTime
		d := time.Duration(p.StartTime) * time.Second
		t = t.Add(d / 2).Round(d)
	}

	return t
}

func (p ProfileConfig) MarshalMpc() ([]byte, error) {
	var start_time int64

	if p.StartTime > 0 {
		start_time = p.Tstart().Unix()
	}

	return []byte(fmt.Sprintf("%d %d %d %d %d %d %d %d",
		p.Direction,
		start_time,
		p.Shallow,
		p.Deep,
		p.StopCheck,
		p.TimeLimit,
		p.ShallowErr,
		p.DeepErr)), nil
}

// Profile back-track parameters (REQBAK command)
type BacktrackConfig struct {
	// Number of back-track attempts when stalled
	Iterations int
	// Back-track rate in (dbar/sec)*1000
	Rate int
	// Back-track time in seconds
	Time int
}
