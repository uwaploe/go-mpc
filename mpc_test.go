package mpc

import (
	"bytes"
	"context"
	"errors"
	"reflect"
	"strings"
	"testing"
	"time"
)

type rwbuffer struct {
	rbuf *bytes.Buffer
	wbuf *bytes.Buffer
}

func (b *rwbuffer) Read(p []byte) (int, error) {
	return b.rbuf.Read(p)
}

func (b *rwbuffer) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestRecv(t *testing.T) {
	resp := []byte("MMPPOS 123456789, 42, 225, 12.1, 1024.21, 2>\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	mpc := NewMPC(&rw, time.Second*3)
	msg, err := mpc.Recv()
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if msg.Cmd != "MMPPOS" {
		t.Fatalf("Message decode error: %v", msg.Cmd)
	}

	_, ok := msg.Params[5].(int64)
	if !ok {
		t.Fatalf("Expected int64, got %T", msg.Params[5])
	}

	v, ok := msg.Params[4].(float64)
	if !ok {
		t.Fatalf("Expected float64, got %T", msg.Params[4])
	}

	if v != float64(1024.21) {
		t.Fatalf("Message decode error: %v", msg.Params)
	}
}

func TestBogus(t *testing.T) {
	resp := []byte("\x00\x00#G0:@@@MMP/ML13317-03/001/01\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	mpc := NewMPC(&rw, time.Second*3)
	msg, err := mpc.Recv()
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if !strings.HasPrefix(msg.Cmd, "#G0:") {
		t.Fatalf("Cannot parse bogus command: %v", msg.Cmd)
	}

}

func TestBTSet(t *testing.T) {
	resp := []byte("ACKBAK 3 45 60>\r\nMMPRDY 123456789>\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	mpc := NewMPC(&rw, time.Second*3)
	cfg, err := mpc.GetBacktrack()
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if cfg.Rate != 45 {
		t.Fatalf("Message decode error: %v", cfg)
	}
}

func TestReady(t *testing.T) {
	resp := []byte("MMPRDY 1429230371>\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	mpc := NewMPC(&rw, time.Second*3)
	s, err := mpc.IsReady()
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if !s {
		t.Fatalf("Message decode error: %v", s)
	}
}

func TestReader(t *testing.T) {
	rbuf := bytes.Buffer{}
	rbuf.Write([]byte("MMPPRF 123456789, 1, 2>\r\n"))
	rbuf.Write([]byte("MMPPOS 123456789, 42, 225, 12.1, 1024.21, 2>\r\n"))
	rbuf.Write([]byte("MMPEND 123456789, 1, 21>\r\n"))
	rbuf.Write([]byte("MMPRDY 123456789>\r\n"))
	rw := rwbuffer{rbuf: &rbuf, wbuf: new(bytes.Buffer)}

	mpc := NewMPC(&rw, time.Second*3)
	ctx := context.Background()
	c, errc := mpc.Reader(ctx, "MMPEND", true)

	var msg Message
	for msg = range c {
	}

	if msg.Cmd != "MMPEND" {
		t.Fatalf("Unexpected final message: %v", msg)
	}

	if err := <-errc; err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

}

func TestSend(t *testing.T) {
	expect := "REQPRF 2 0 0 0 5 900 0 0\r\n"
	buf := new(bytes.Buffer)

	mpc := NewMPC(buf, time.Second*3)
	cfg := ProfileConfig{
		Direction: PROFILE_STATIONARY,
		StopCheck: 5,
		TimeLimit: 900}
	err := mpc.Send("REQPRF", cfg)
	if err != nil {
		t.Fatal(err)
	}

	val := string(buf.Bytes())
	if expect != val {
		t.Errorf("Bad value; expected %q, got %q", expect, val)
	}

}

func TestDecode(t *testing.T) {
	table := []struct {
		in, cmd  string
		obj, out interface{}
		rec      map[string]interface{}
		err      error
	}{
		{
			in:  "MMPPOS 1630525308, 711, 0, 11.8, 2639.45, 2 >\r\n",
			cmd: "MMPPOS",
			obj: &Position{},
			out: &Position{T: 1630525308, Pnum: 711,
				Current: 0, Vbatt: 11.8, Pressure: 2639.45, Mode: PROFILE_STATIONARY},
			rec: map[string]interface{}{
				"pnum":     int64(711),
				"current":  int64(0),
				"vbatt":    float64(11.8),
				"pressure": float64(2639.45),
				"mode":     ProfileType(2).String(),
			},
		},
		{
			in:  "MMPDCK 1630525320, 2639.44 >\r\n",
			cmd: "MMPDCK",
			obj: &ProfDock{},
			out: &ProfDock{T: 1630525320, Pressure: 2639.44},
			rec: map[string]interface{}{
				"t":  int64(1630525320),
				"pr": float64(2639.44),
			},
		},
		{
			in:  "MMPEND 1630525320, 711, 21 >\r\n",
			cmd: "MMPEND",
			obj: &ProfEnd{},
			out: &ProfEnd{T: 1630525320, Pnum: 711, Status: 21},
			rec: map[string]interface{}{
				"t":      int64(1630525320),
				"pnum":   int64(711),
				"status": int64(21),
			},
		},
		{
			in:  "ACKPRF 2 1630437275 0 0 5 900 0 0>\r\n",
			cmd: "ACKPRF",
			obj: &ProfileConfig{},
			out: &ProfileConfig{
				Direction: PROFILE_STATIONARY,
				StartTime: 1630437275,
				StopCheck: 5,
				TimeLimit: 900,
			},
		},
		{
			in:  "ACKPRF 2 1630437275 0 0 xx 900 0 0>\r\n",
			cmd: "ACKPRF",
			obj: &ProfileConfig{},
			err: ErrInvalidField,
		},
	}

	var buf bytes.Buffer
	mpc := NewMPC(&buf, time.Second*3)
	for _, e := range table {
		buf.Write([]byte(e.in))
		msg, err := mpc.Recv()
		if err != nil {
			t.Fatal(err)
		}

		err = UnmarshalMpc(msg, e.obj)
		if !errors.Is(err, e.err) {
			t.Fatalf("Unexpected error value: %v", err)
		}
		if err != nil {
			continue
		}

		if msg.Cmd != e.cmd {
			t.Errorf("Bad value; expected %q, got %q", e.cmd, msg.Cmd)
		}

		if !reflect.DeepEqual(e.obj, e.out) {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, e.obj)
		}

		if e.rec != nil {
			if m, ok := e.obj.(Mapper); ok {
				rec := m.AsMap()
				if !reflect.DeepEqual(e.rec, rec) {
					t.Errorf("Bad value; expected %#v, got %#v", e.rec, rec)
				}
			}
		}
		buf.Reset()
	}
}
