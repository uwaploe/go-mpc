package mpc

import (
	"errors"
	"fmt"
	"reflect"
)

type MpcMarshaler interface {
	MarshalMpc() ([]byte, error)
}

type MpcUnmarshaler interface {
	UnmarshalMpc(Message) error
}

var ErrInvalidField = errors.New("Invalid message field")

// Set a reflect.Value from the contents of the src. Only integers
// (signed and unsigned) and floats are supported. If the value
// is not changeable, this function will panic.
func setValue(v reflect.Value, src interface{}) error {
	vsrc := reflect.ValueOf(src)
	switch v.Kind() {
	case reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		switch vsrc.Kind() {
		case reflect.Int64:
			v.SetUint(uint64(vsrc.Int()))
		case reflect.Float64:
			v.SetUint(uint64(vsrc.Float()))
		default:
			return ErrInvalidField
		}
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Int:
		switch vsrc.Kind() {
		case reflect.Int64:
			v.SetInt(vsrc.Int())
		case reflect.Float64:
			v.SetInt(int64(vsrc.Float()))
		default:
			return ErrInvalidField
		}
	case reflect.Float32, reflect.Float64:
		switch vsrc.Kind() {
		case reflect.Int64:
			v.SetFloat(float64(vsrc.Int()))
		case reflect.Float64:
			v.SetFloat(vsrc.Float())
		default:
			return ErrInvalidField
		}
	default:
		return fmt.Errorf("Unsupported type: %s", v.Kind())
	}
	return nil
}

func UnmarshalMpc(m Message, v interface{}) error {
	if um, ok := v.(MpcUnmarshaler); ok {
		return um.UnmarshalMpc(m)
	}

	val := reflect.ValueOf(v)

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	if val.Kind() == reflect.Interface {
		val = reflect.ValueOf(val.Interface())
	}

	if val.Kind() != reflect.Struct {
		return errors.New("Can only unmarshal into structs")
	}

	j := int(0)
	nf := len(m.Params)

	for i := 0; i < val.NumField(); i++ {
		if fv := val.Field(i); fv.CanSet() {
			t := val.Type()
			if t.Field(i).Name == "MpcResp" && fv.Kind() == reflect.String {
				fv.SetString(m.Cmd)
				continue
			}
			// Skip any fields tagged with "-"
			if t.Field(i).Tag.Get("mpc") == "-" {
				continue
			}

			if j < nf {
				if err := setValue(fv, m.Params[j]); err != nil {
					return fmt.Errorf("Cannot set field %d (%s): %w", i,
						t.Field(i).Name,
						err)
				}
				j++
			}
		}
	}

	return nil
}
